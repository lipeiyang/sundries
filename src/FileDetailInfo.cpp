//#include "stdafx.h"

#include "FileDetailInfo.h"

#include <windows.h>

#include <Strsafe.h>

#include <Softpub.h>
#include <wincrypt.h>
#include <wintrust.h>

#pragma comment(lib, "Version.lib")
#pragma comment(lib, "Wintrust.lib")
#pragma comment(lib, "Crypt32.lib")


#include <memory>


#define ENCODING (X509_ASN_ENCODING | PKCS_7_ASN_ENCODING)

const wchar_t *kVersion_Infos[] = {
	L"Comments",
	L"CompanyName",
	L"FileDescription",
	L"FileVersion",
	L"InternalName",
	L"LegalCopyright",
	L"LegalTrademarks",
	L"OriginalFilename",
	L"ProductName",
	L"ProductVersion",
	L"PrivateBuild",
	L"SpecialBuild"
};

#define BUF_SIZE 512

static std::map<FileDetailInfo::eInfoId, std::wstring> InitResourceInfo(const std::wstring& pefile)
{
	int    iRet = 0;
	std::map<FileDetailInfo::eInfoId, std::wstring>  emt;
	DWORD dwVersionSize = 0;

	do
	{
		dwVersionSize = GetFileVersionInfoSize(pefile.c_str(), NULL);
		if (dwVersionSize == 0)
			break;

		std::unique_ptr<wchar_t[]> VersionBuff(new wchar_t[dwVersionSize]);
		if (!GetFileVersionInfo(pefile.c_str(), 0, dwVersionSize, (void*)VersionBuff.get())) {
			break;
		}

		struct LANGANDCODEPAGE {
			WORD wLanguage;
			WORD wCodePage;
		} *lpTranslate;

		UINT    cbTranslate = 0;

		if (!VerQueryValue(VersionBuff.get(),
			L"\\VarFileInfo\\Translation",
			(LPVOID*)&lpTranslate,
			&cbTranslate)){
			break;
		}
		auto maxLang = (cbTranslate / sizeof(struct LANGANDCODEPAGE));
		bool ok = true;
		for (UINT i = 0; i < maxLang; i++) {
			for (int j = 0; j < _countof(kVersion_Infos); j++) {

				wchar_t SubBlock[BUF_SIZE] = {};
				wchar_t *lpBuffer = NULL;
				UINT dwBytes = 0;

				auto hr = StringCchPrintf(SubBlock, BUF_SIZE,
					L"\\StringFileInfo\\%04x%04x\\%s",
					lpTranslate[i].wLanguage,
					lpTranslate[i].wCodePage,
					kVersion_Infos[j]);
				if (FAILED(hr)) {
					ok = false;
					break;
				}

				// Retrieve file description for language and code page "i". 
				if (!VerQueryValue(VersionBuff.get(),
					SubBlock,
					(void**)&lpBuffer,
					&dwBytes)){
					continue;
				}

				//std::wstring key = kVersion_Infos[j];
				std::wstring value = lpBuffer == NULL ? L"" : lpBuffer;
				emt[(FileDetailInfo::eInfoId)j] = value;
			}

			if (ok)
				break;
			ok = true;
		}
	} while (false);

	return std::move(emt);
}



static std::wstring GetSignature(const std::wstring& pe)
{
	HCERTSTORE hStore = NULL;
	HCRYPTMSG hMsg = NULL;
	PCCERT_CONTEXT pCertContext = NULL;

	std::wstring szName;
	do
	{
		BOOL fResult;
		DWORD dwEncoding, dwContentType, dwFormatType;
		DWORD dwSignerInfo;

		//CertCloseStore(hStore)
		//CryptMsgClose(hMsg)
		fResult = CryptQueryObject(CERT_QUERY_OBJECT_FILE,
			pe.c_str(),
			CERT_QUERY_CONTENT_FLAG_PKCS7_SIGNED_EMBED,
			CERT_QUERY_FORMAT_FLAG_BINARY,
			0,
			&dwEncoding,
			&dwContentType,
			&dwFormatType,
			&hStore,
			&hMsg,
			NULL);
		if (!fResult) {
			break;
		}


		// Get signer information size.
		fResult = CryptMsgGetParam(hMsg,
			CMSG_SIGNER_INFO_PARAM,
			0,
			NULL,
			&dwSignerInfo);
		if (!fResult) {
			break;
		}

		std::unique_ptr<byte[]> buffer(new byte[dwSignerInfo]);
		auto pSignerInfo = (PCMSG_SIGNER_INFO)buffer.get();
		// Get Signer Information.
		fResult = CryptMsgGetParam(hMsg,
			CMSG_SIGNER_INFO_PARAM,
			0,
			(PVOID)pSignerInfo,
			&dwSignerInfo);
		if (!fResult) {
			break;
		}

		// Search for the signer certificate in the temporary 
		// certificate store.
		CERT_INFO CertInfo = {};
		CertInfo.Issuer = pSignerInfo->Issuer;
		CertInfo.SerialNumber = pSignerInfo->SerialNumber;

		//CertFreeCertificateContext
		pCertContext = CertFindCertificateInStore(hStore,
			ENCODING,
			0,
			CERT_FIND_SUBJECT_CERT,
			(PVOID)&CertInfo,
			NULL);
		if (!pCertContext) {
			break;
		}

		//LPTSTR szName = NULL;
		DWORD dwData;
		if (!(dwData = CertGetNameString(pCertContext,
			CERT_NAME_SIMPLE_DISPLAY_TYPE,
			0,
			NULL,
			NULL,
			0))) {
			break;
		}
		szName.resize(dwData);
		// Get subject name.
		if (!(CertGetNameString(pCertContext,
			CERT_NAME_SIMPLE_DISPLAY_TYPE,
			0,
			NULL,
			(wchar_t*)szName.c_str(),
			dwData))){
			break;
		}
		break;
	} while (false);

	if (pCertContext)
		CertFreeCertificateContext(pCertContext);
	if (hMsg)
		CryptMsgClose(hMsg);
	if (hStore)
		CertCloseStore(hStore, CERT_CLOSE_STORE_FORCE_FLAG);


	return szName;
}

static bool ValideSignature(const std::wstring& pe)
{
	bool rt = false;


	WINTRUST_FILE_INFO FileData = { sizeof(WINTRUST_FILE_INFO), pe.c_str() };

	GUID WVTPolicyGUID = WINTRUST_ACTION_GENERIC_VERIFY_V2;
	WINTRUST_DATA WinTrustData = {};
	WinTrustData.cbStruct = sizeof(WinTrustData);
	// Use default code signing EKU.
	WinTrustData.pPolicyCallbackData = NULL;
	// No data to pass to SIP.
	WinTrustData.pSIPClientData = NULL;
	// Disable WVT UI.
	WinTrustData.dwUIChoice = WTD_UI_NONE;
	// No revocation checking.
	WinTrustData.fdwRevocationChecks = WTD_REVOKE_NONE;
	// Verify an embedded signature on a file.
	WinTrustData.dwUnionChoice = WTD_CHOICE_FILE;
	// Verify action.
	WinTrustData.dwStateAction = WTD_STATEACTION_VERIFY;
	// Verification sets this value.
	WinTrustData.hWVTStateData = NULL;
	// Not used.
	WinTrustData.pwszURLReference = NULL;
	// This is not applicable if there is no UI because it changes 
	// the UI to accommodate running applications instead of 
	// installing applications.
	WinTrustData.dwUIContext = 0;
	// Set pFile.
	WinTrustData.pFile = &FileData;

	auto lStatus = WinVerifyTrust(
		NULL,
		&WVTPolicyGUID,
		&WinTrustData);
	DWORD dwLastError = 0;
	switch (lStatus)
	{
	case ERROR_SUCCESS:
		/*
		Signed file:
		- Hash that represents the subject is trusted.

		- Trusted publisher without any verification errors.

		- UI was disabled in dwUIChoice. No publisher or
		time stamp chain errors.

		- UI was enabled in dwUIChoice and the user clicked
		"Yes" when asked to install and run the signed
		subject.
		*/
		rt = true;
		break;

	case TRUST_E_NOSIGNATURE:
		// The file was not signed or had a signature 
		// that was not valid.

		// Get the reason for no signature.
		dwLastError = GetLastError();
		if (TRUST_E_NOSIGNATURE == dwLastError ||
			TRUST_E_SUBJECT_FORM_UNKNOWN == dwLastError ||
			TRUST_E_PROVIDER_UNKNOWN == dwLastError)
		{
			// The file was not signed.
		}
		else
		{
			// The signature was not valid or there was an error 
			// opening the file.
		}

		break;

	case TRUST_E_EXPLICIT_DISTRUST:
		// The hash that represents the subject or the publisher 
		// is not allowed by the admin or user.
		break;

	case TRUST_E_SUBJECT_NOT_TRUSTED:
		// The user clicked "No" when asked to install and run.
		break;

	case CRYPT_E_SECURITY_SETTINGS:
		/*
		The hash that represents the subject or the publisher
		was not explicitly trusted by the admin and the
		admin policy has disabled user trust. No signature,
		publisher or time stamp errors.
		*/
		break;

	default:
		// The UI was disabled in dwUIChoice or the admin policy 
		// has disabled user trust. lStatus contains the 
		// publisher or time stamp chain error.
		break;
	}

	// Any hWVTStateData must be released by a call with close.
	WinTrustData.dwStateAction = WTD_STATEACTION_CLOSE;
	lStatus = WinVerifyTrust(
		NULL,
		&WVTPolicyGUID,
		&WinTrustData);

	return rt;
}


//////////////////////////////////////////////////////////////////////////
FileDetailInfo::FileDetailInfo(const std::wstring& pefile)
:_file(pefile)
{
	_mVerInfo = InitResourceInfo(_file);
	_mVerInfo[INFO_Signature] = GetSignature(_file);
}

FileDetailInfo::~FileDetailInfo()
{
}

std::wstring FileDetailInfo::GetInfo(eInfoId id)
{
	auto itr = _mVerInfo.find(id);
	if(itr != _mVerInfo.end())
		return itr->second;
	return L"";
}

bool FileDetailInfo::ValideSignature()
{
	return ::ValideSignature(_file);
}
