#pragma once

#include <algorithm>
#include <typeinfo>

class Any
{
public: // structors

	Any()
		: content(0)
	{
	}

	template<typename ValueType>
	Any(const ValueType & value)
		: content(new holder<ValueType>(value))
	{
	}

	Any(const Any & other)
		: content(other.content ? other.content->clone() : 0)
	{
	}

	~Any()
	{
		delete content;
	}

public: // modifiers

	Any & swap(Any & rhs)
	{
		std::swap(content, rhs.content);
		return *this;
	}

	template<typename ValueType>
	Any & operator=(const ValueType & rhs)
	{
		Any(rhs).swap(*this);
		return *this;
	}

	Any & operator=(Any rhs)
	{
		rhs.swap(*this);
		return *this;
	}

public: // queries

	bool empty() const
	{
		return !content;
	}

	const std::type_info & type() const
	{
		return content ? content->type() : typeid(void);
	}

public:
	template <typename T> bool Is() {
		return contains_t<T>()(*this);
	}


	template<typename ValueType>
	ValueType Get()const
	{
		typedef typename remove_reference<ValueType>::type nonref;

		nonref * result = this->type() == typeid(nonref)
			? &static_cast<Any::holder<nonref> *>(this->content)->held
			: 0;
		if (!result)
			throw("Failed to cast any to type.");
		return *result;
	}

	template<typename ValueType>
	ValueType Get(const ValueType& def, bool* ok = nullptr)const
	{
		typedef typename remove_reference<ValueType>::type nonref;

		nonref * result = this->type() == typeid(nonref)
			? &static_cast<Any::holder<nonref> *>(this->content)->held
			: 0;

		if (ok){
			*ok = result ? true: false;
		}
		if (!result){
			return def;
		}
		return *result;
	}

private: 
	template <typename T> struct contains_t {
		typedef Any argument_type;
		typedef bool result_type;
		bool operator()(Any a) const {
			return typeid(T) == a.type();
		}
	};

	template <typename T> contains_t<T> contains() {
		return contains_t<T>();
	}

private: // types
	class placeholder
	{
	public: // structors

		virtual ~placeholder()
		{
		}

	public: // queries

		virtual const std::type_info & type() const = 0;

		virtual placeholder * clone() const = 0;

	};

	template<typename ValueType>
	class holder : public placeholder
	{
	public: // structors

		holder(const ValueType & value)
			: held(value)
		{
		}

	public: // queries

		virtual const std::type_info & type() const
		{
			return typeid(ValueType);
		}

		virtual placeholder * clone() const
		{
			return new holder(held);
		}

	public: // representation

		ValueType held;

	private: // intentionally left unimplemented
		holder & operator=(const holder &);
	};


private: // representation

// 	template<typename ValueType>
// 	friend ValueType * Any_cast(Any *);
// 
// 	template<typename ValueType>
// 	friend ValueType * unsafe_Any_cast(Any *);


	placeholder * content;

};
// 
// class bad_Any_cast : public std::bad_cast
// {
// public:
// 	virtual const char * what() const throw()
// 	{
// 		return "boost::bad_Any_cast: "
// 			"failed conversion using boost::Any_cast";
// 	}
// };
// 
// template<typename ValueType>
// ValueType * Any_cast(Any * operand)
// {
// 	return operand &&
// 		operand->type() == typeid(ValueType)
// 		? &static_cast<Any::holder<ValueType> *>(operand->content)->held
// 		: 0;
// }
// 
// template<typename ValueType>
// inline const ValueType * Any_cast(const Any * operand)
// {
// 	return Any_cast<ValueType>(const_cast<Any *>(operand));
// }
// 
// template<typename ValueType>
// ValueType Any_cast(Any & operand)
// {
// 	typedef typename remove_reference<ValueType>::type nonref;
// 
// 	nonref * result = Any_cast<nonref>(&operand);
// 	if (!result)
// 		throw(bad_Any_cast());
// 	return *result;
// }
// 
// template<typename ValueType>
// inline ValueType Any_cast(const Any & operand)
// {
// 	typedef typename remove_reference<ValueType>::type nonref;
// 
// 	return Any_cast<const nonref &>(const_cast<Any &>(operand));
// }
// 
// // Note: The "unsafe" versions of Any_cast are not part of the
// // public interface and may be removed at Any time. They are
// // required where we know what type is stored in the Any and can't
// // use typeid() comparison, e.g., when our types may travel across
// // different shared libraries.
// template<typename ValueType>
// inline ValueType * unsafe_Any_cast(Any * operand)
// {
// 	return &static_cast<Any::holder<ValueType> *>(operand->content)->held;
// }
// 
// template<typename ValueType>
// inline const ValueType * unsafe_Any_cast(const Any * operand)
// {
// 	return unsafe_Any_cast<ValueType>(const_cast<Any *>(operand));
// }




// test
// #include "Any.hpp"
// 
// void TestAny(){
// 	Any sss = 1212.0f;
// 
// 	sss = AAAA(99);
// 
// 	if (sss.Is<AAAA>()){
// 		auto aa = sss.Get<AAAA>();
// 	}
// 
// 	sss = 0;
// 
// 	bool ok = false;
// 	auto aa = sss.Get<AAAA>(AAAA(20), &ok);
// 
// 	return;
// }
// 
