#pragma once


#include <map>

class FileDetailInfo
{
public:
// 	const wchar_t *kVersion_Infos[] = {
// 		L"Comments",
// 		L"CompanyName",
// 		L"FileDescription",
// 		L"FileVersion",
// 		L"InternalName",
// 		L"LegalCopyright",
// 		L"LegalTrademarks",
// 		L"OriginalFilename",
// 		L"ProductName",
// 		L"ProductVersion",
// 		L"PrivateBuild",
// 		L"SpecialBuild"
// 	};
	enum eInfoId
	{
		INFO_Begin,
		INFO_Comments = INFO_Begin,
		INFO_CompanyName,
		INFO_FileDescription,
		INFO_FileVersion,
		INFO_InternalName,
		INFO_LegalCopyright,
		INFO_LegalTrademarks,
		INFO_OriginalFilename,
		INFO_ProductName,
		INFO_ProductVersion,
		INFO_PrivateBuild,
		INFO_SpecialBuild,
		INFO_DetailEnd,
		INFO_Signature = INFO_DetailEnd,
		INFO_END
	};
public:
	FileDetailInfo(const std::wstring& pefile);
	~FileDetailInfo();

	std::wstring GetInfo(eInfoId id);

	bool ValideSignature();

private:
	std::wstring	_file;
	std::map<eInfoId, std::wstring> _mVerInfo;
};