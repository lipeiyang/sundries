#pragma once

template<size_t _DIM, typename _T = float>
struct Vector{
	static_assert(_DIM > 0, "_DIM must lager than 1.");

	typedef Vector<_DIM, _T> _ThisType;

	_T _x;
	Vector<_DIM - 1, _T> _x1;


	_T& operator[](size_t i){
		return (&_x)[i];
	}
	//  _T& operator[](size_t i){
	//  return i==0?_x:_x1[i - 1];
	//  }

	_ThisType& operator += (const _T& x){
		this->_x += x;
		this->_x1 += x;
		return *this;
	}


	_ThisType& operator -= (const _T& x){
		this->_x -= x;
		this->_x1 -= x;
		return *this;
	}


	_ThisType& operator *= (const _T& x){
		this->_x *= x;
		this->_x1 *= x;
		return *this;
	}


	_ThisType& operator /= (const _T& x){
		this->_x /= x;
		this->_x1 /= x;
		return *this;
	}


	_ThisType& operator += (const _ThisType& rhs){
		this->_x += rhs._x;
		this->_x1 += rhs._x1;
		return *this;
	}


	_ThisType& operator -= (const _ThisType& rhs){
		this->_x -= rhs._x;
		this->_x1 -= rhs._x1;
		return *this;
	}


	//==, we'd better compare _x with some Tolerance |lhs._x - rhs._x| < xxxx
	friend bool operator == (const _ThisType& lhs, const _ThisType& rhs){
		return lhs._x == rhs._x && lhs._x1 == lhs._x1;
		//return Distance(lhs, rhs) <= somevalue
	}


	//add
	friend _ThisType operator + (const _ThisType& lhs, const _ThisType& rhs){
		return{ lhs._x + rhs._x, lhs._x1 + rhs._x1 };
	}
	//add value
	friend _ThisType operator + (const _ThisType& lhs, const _T& rhs){
		return{ lhs._x + rhs, lhs._x1 + rhs };
	}
	//value add
	friend _ThisType operator + (const _T& lhs, const _ThisType& rhs){
		return rhs + lhs;
	}
	//minor
	friend _ThisType operator - (const _ThisType& lhs, const _ThisType& rhs){
		return{ lhs._x - rhs._x, lhs._x1 - rhs._x1 };
	}
	//minor value
	friend _ThisType operator - (const _ThisType& lhs, const _T& rhs){
		return{ lhs._x - rhs, lhs._x1 - rhs };
	}
	//value minor
	friend _ThisType operator - (const _T& lhs, const _ThisType& rhs){
		return{ lhs - rhs._x, lhs - rhs._x1 };
	}
	//negative
	friend _ThisType operator - (const _ThisType& rhs){
		return{ -rhs._x, -rhs._x1 };
	}


	//dot product
	friend _T operator * (const _ThisType& lhs, const _ThisType& rhs){
		return lhs._x * rhs._x + lhs._x1 * rhs._x1;
	}

	//value product/ value / scale
	friend _ThisType operator * (const _ThisType& lhs, const _T& rhs){
		return{ lhs._x * rhs, lhs._x1 * rhs };
	}
	friend _ThisType operator * (const _T& lhs, const _ThisType& rhs){
		return rhs * lhs;
	}

	friend _ThisType operator/(const _ThisType& lhs, const _T& rhs){
		return lhs * (1 / rhs);
	}


	//////////////////////////////////////////////////////////////////////////


	_T Distance(const _ThisType& rhs){
		auto d = (*this - rhs);
		return std::sqrt(d * d);
	}
	_T Module(){
		return std::sqrt(*this * *this);
	}


	_ThisType Project(const _ThisType& rhs){
		//if rhs is zero
		return rhs * (*this * rhs) / (rhs * rhs);
	}


	//no normal zero
	_ThisType Normal(){
		return *this / Module();
	}


	_ThisType& Normalize(){
		*this /= Module();
		return *this;
	}


	template<int i>
	static _ThisType Base(){
		static_assert(i < _DIM, "base more than dimension");
		_ThisType v = {};
		v[i] = 1;
		return v;
	}
	static _ThisType Zero(){
		return{};
	}
};


template<typename _T>
struct Vector<1, _T>{
	typedef Vector<1, _T> _ThisType;

	_T _x;
	//i must be zero
	//  _T& operator[](size_t i){
	//  return _x;
	//  }

	_ThisType& operator += (const _T& x){
		this->_x += x;
		return *this;
	}


	_ThisType& operator -= (const _T& x){
		this->_x -= x;
		return *this;
	}


	_ThisType& operator *= (const _T& x){
		this->_x *= x;
		return *this;
	}


	_ThisType& operator /= (const _T& x){
		this->_x /= x;
		return *this;
	}


	_ThisType& operator += (const _ThisType& rhs){
		this->_x += rhs._x;
		return *this;
	}
	_ThisType& operator -= (const _ThisType& rhs){
		this->_x -= rhs._x;
		return *this;
	}


	//==
	friend _ThisType operator == (const _ThisType& lhs, const _ThisType& rhs){
		return lhs._x == rhs._x;
	}


	//add
	friend _ThisType operator + (const _ThisType& lhs, const _ThisType& rhs){
		return{ lhs._x + rhs._x };
	}
	//add value
	friend _ThisType operator + (const _ThisType& lhs, const _T& rhs){
		return{ lhs._x + rhs };
	}

	//minor
	friend _ThisType operator - (const _ThisType& lhs, const _ThisType& rhs){
		return{ lhs._x - rhs._x };
	}
	//minor value
	friend _ThisType operator - (const _ThisType& lhs, const _T& rhs){
		return{ lhs._x - rhs };
	}


	//value minor
	friend _ThisType operator - (const _T& lhs, const _ThisType& rhs){
		return{ lhs - rhs._x };
	}


	//negative
	friend _ThisType operator - (const _ThisType& rhs){
		return{ -rhs._x };
	}


	//dot product
	friend _T operator * (const _ThisType& lhs, const _ThisType& rhs){
		return{ lhs._x * rhs._x };
	}


	//value product/ value / scale
	friend _ThisType operator * (const _ThisType& lhs, const _T& rhs){
		return{ lhs._x * rhs };
	}
};





///////////////////////////////////////////////////////////////////////////
// test
// 
//#include "Vector.hpp"
//void TestVector(){
// 
// 
// 	Vector<3> v1 = { 3, 4, 5 };//
// 	Vector<3> v2 = { 1, 0, 0 };
// 	Vector<3> v3 = Vector<3>::Base<1>();
// 	auto nv2 = v1.Normal();
// 	auto v2c = v1;
// 	v2c.Normalize();
// 
// 
// 	Vector<3> v0 = Vector<3>::Zero();
// 
// 
// 	auto v = v1 + v2;
// 	auto f = v1 * v2;
// 
// 
// 	v = v1 - v2;
// 	v = v1 - 2;
// 	v = 2 - v1;
// 	v = v1 * 2;
// 	v = 2 * v1;
// 
// 
// 	v = v1 / 0;
// 
// 
// 	v = v1.Project(v2);
// 
// 
// 	v += v1;
// 	v -= v1;
// 
// 
// 	v += 1;
// 	v -= 1;
// 
// 	v = v1;
// 	v *= 2;
// 	v /= 2;
// 
// 	f = v1.Distance(v2);
// 
// 	f = v1.Module();
// 	return;
//}